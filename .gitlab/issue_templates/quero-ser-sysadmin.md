- Qual é o seu nome completo?

- Qual é a sua identidade de gênero? (opcional)

- Qual é a sua cidade e seu estado?

- Qual o link da sua conta no [mozillians.org](https://mozillians.org)?

- Insira o nome e o perfil da(s) pessoa(s) que pode(m) atestar suas contribuições na comunidade.

- Descreva o que você faz atualmente na comunidade e/ou para Mozilla

- É a primeira vez que você se candidata para juntar-se ao time como SysAdmin?

- Insira outras informações adicionais que você acha relevante (opcional)

- [ ] Você declara estar ciente de todos os requisitos, inclusive dos conhecimentos exigidos e o [Acordo de Confidencialidade — SysAdmin](https://).

**Obs: Marque o checkbox "*This issue is confidential and should only be visible to team members with at least Reporter access.*" para manter os seus dados protegidos.**