- Qual o nome do serviço com problema?

- Descreva o que está acontecendo. (Adicione capturas de tela, se possível)

  - o que você fez?

  - o que aconteceu?

  - o que deveria ter acontecido?

- Há quanto tempo o serviço não está funcionando?
  - [ ] menos de 1 hora
  - [ ] de 1 a 2 horas
  - [ ] de 2 a 4 horas
  - [ ] 5 horas ou mais 
